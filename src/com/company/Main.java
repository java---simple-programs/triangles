package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        System.out.println("Podaj trzy długości boków, by sprawdzić, czy można z nich zbdować trójkąt. Po każdym naciśnij ENTER.");
        Integer x=scan.nextInt();
        Integer y=scan.nextInt();
        Integer z=scan.nextInt();

        if(x+y>z && x+z>y && y+z>x){
            System.out.println("Można utworzyć trójkąt :)");
            if(x==y && y==z){
                System.out.println("Jest to trójkąt równoboczny");
            }
            else if((x==y && x+y>z) || (y==z && y+z>x) || (x==z && x+z>y)){
                System.out.println("Jest to trójkąt równoramienny");
            }
            else if((x*x+y*y==z*z) || (x*x+z*z==y*y) || (y*y+z*z==x*x)){
                System.out.println("Jest to trójkąt prostokątny");
            }
            else{
                System.out.println("Nie da się określić");
            }

        } else{
            System.out.println("Z tych długości nie można utowrzyć trójkąta :(");
        }


    }
}
